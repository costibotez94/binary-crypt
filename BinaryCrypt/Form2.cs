﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace BinaryCrypt
{
    public partial class Form2 : Form
    {
        int k = 0;
        bool closing = false;
        public Form2()
        {
            InitializeComponent();
            panel2.Location = new Point();
            panel3.Location = new Point();
            panel4.Location = new Point();
            this.Size = new Size(457, 376);
        }


        private void button1_Click(object sender, EventArgs e)
        {
            label27.Visible = false;
            if (k == 0)
            {
                panel1.Visible = false;
                panel2.Visible = true;
                buttonNext.Text = "I Agree";
                buttonBack.Visible = true;
            }
            if (k == 1)
            {
                panel2.Visible = false;
                panel3.Visible = true;
                buttonNext.Text = "Install";
            }
            if (k == 2)
            {
                timer.Enabled = true;
                panel3.Visible = false;
                panel4.Visible = true;
                buttonNext.Enabled = false;
                buttonBack.Enabled = false;
            }
            if (k == 3)
            {
                this.Close();
                closing = true;
            }
                k++;
        }

        private void button4_Click(object sender, EventArgs e)
        {

            k--;
            if (k == 0)
            {
                label27.Visible = true;
                panel1.Visible = true;
                panel2.Visible = false;
                buttonNext.Text = "Next";
                buttonBack.Visible = false;
            }
            if (k == 1)
            {
                panel2.Visible = true;
                panel3.Visible = false;
                buttonNext.Text = "I Agree";   
            }
            if (k == 2)
            {
                panel3.Visible = true;
                panel4.Visible = false;
                buttonNext.Text = "Install";
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult fileOpen = folderBrowserDialog1.ShowDialog();
            if (fileOpen == DialogResult.OK)
            {
                textBox1.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (k == 3)
            {
                closing = true;
                this.Close();
                return;
            }
            DialogResult msgBoxAsk = MessageBox.Show("Are you sure you want to quit Binary Crypt Setup?",
                                                     "Binary Crypt Setup", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (msgBoxAsk == DialogResult.Yes)
            {
                closing = true;
                this.Close();
            }
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {

            if (e.CloseReason != CloseReason.WindowsShutDown && !(closing))
            {

                DialogResult msgBoxAsk = MessageBox.Show("Are you sure you want to quit Binary Crypt Setup?",
                                                     "Binary Crypt Setup", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (msgBoxAsk == DialogResult.Yes)
                {
                    closing = true;
                    this.Close();
                }
                if (!(closing))
                {
                    e.Cancel = true;
                }
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (progressBar1.Value != 50)
                progressBar1.Value++;
            else
            {
                timer.Enabled = false;
               File.WriteAllBytes(textBox1.Text + "\\Binary Crypt.exe", Properties.Resources.BinaryCrypt);
                panel4.Visible = false;
                panel1.Visible = true;
                label27.Visible = true;
                buttonCancel.Text = "Finish";
                buttonNext.Visible = false;
                buttonBack.Visible = false;
                label3.Text = "Setup has finished installing Binary Crypt on your computer.\n" +
                            "Click Finish to exit Setup.";
            }

        }

      
    }
}