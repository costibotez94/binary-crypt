﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BinaryCrypt_Program
{
    public partial class Form2 : Form
    {
        string message;
        int i, ok = 0;
        public Form2()
        {
            InitializeComponent();
            this.Size=new Size(269, 454);
        }

        private void buttonCrypt_Click(object sender, EventArgs e)
        {
            textBox2.Clear();
            labelCrypting.Visible = true;
            message = textBox1.Text;
            labelCrypting.Text = "Crypting...";
            labelCrypting.Visible = true;
            toolStripProgressBar1.Value = 0;
            timerCrypting.Enabled = true;
            //timer.Start();
            // MessageBox.Show(message.Split().Length.ToString());
            //  for (i = 0; i < message.Length; i++)
            //    if ((int.Parse(message.Substring(i, 1))) <= 9 && (int.Parse(message.Substring(i, 1)) >= 0))      //daca e cifra
            //        message_crypted = binar(int.Parse(message.Substring(i, 1))); 
        }

        public int binar(int n)
        {
            int aux = 0, nr = 0, l = 0;

            do
            {
                aux = aux * 10 + n % 2;
                n /= 2;
                l++;
            }
            while (n != 0);

            do
            {
                nr = nr * 10 + aux % 10;
                aux /= 10;
                --l;
            }
            while (l!=0);

            return nr;
        }

        public void zecimal(int nr)
        {
            int aux = 0, n = 0;
            char c;
            if (nr == 0) return;
            while (nr!=0)
            {
                if ((nr % 10 == 0) || (nr % 10 == 1))
                    n = n + (nr % 10) * (int)Math.Pow(2, aux++);
                nr /= 10;
            }
            c = (char)(n);
            if (c == '\0' && ok==0)		//valideaza criptul
            {
               textBox3.Text= "Eroare cript";
                ok = 1;
            }
            else
                if (ok==0)				//reducerea mesajului de eroare
                    textBox3.Text = textBox3.Text + (char)(n);
            ok = 0;
        }

        private void cryptAMessageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripProgressBar1.Visible = true;
            toolStripStatusLabel1.Margin = new System.Windows.Forms.Padding(0);
            pictureBoxLogo.Visible = false;
            this.Size = new Size(444, 410);
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            panelCrypt.Visible = true;
            panelDecrypt.Visible = false;
            panelCrypt.Location = new Point(6, 27);
            toolStripProgressBar1.Value = 0;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void decryptMessageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripProgressBar1.Visible = true;
            toolStripStatusLabel1.Margin = new System.Windows.Forms.Padding(0);
            pictureBoxLogo.Visible = false;
            this.Size = new Size(444, 410);
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            panelDecrypt.Location = new Point(6, 27);
            panelDecrypt.Visible = true;
            panelCrypt.Visible = false;
            toolStripProgressBar1.Value = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox3.Clear();
            message = textBox4.Text;
            message = message.Replace(" ", "");
            message = message.Replace("\r\n", "");

            labelDecrypting.Text = "Decrypting...";
            labelDecrypting.Visible = true;
            toolStripProgressBar1.Value = 0;
            timerDecrypting.Enabled = true;

           // foreach(char c in message)
           //     if(char.IsPunctuation(c) || char.IsSymbol(c) || char.IsLetterOrDigit(c))
           //       decript[i]=decript[i]+c;

        }

        private void timer_Tick(object sender, EventArgs e)
        {
            while (toolStripProgressBar1.Value < toolStripProgressBar1.Maximum)
                toolStripProgressBar1.Value++;

            timerCrypting.Enabled = false;
            labelCrypting.Text = "Done!";
            textBox2.Visible = true;
            int[] message_code = new int[message.Length];
            int k = message.Length, j = 1, l = 0, verif;

            for (i = 0; i < message.Length; i++)
            {
                message_code[i] = binar((int)message[i]);
            }

            for (i = 0; i < message.Length; i++)
            {
                if (l % 8 == 0 && l != 0)			                                	//imparte criptul in linii
                    textBox2.Text = textBox2.Text + "\r\n";

                if (k > 8 && k % 8 != 0)  		                                    //insereaza 0-uri
                {
                    j = 2;
                    if (l % j != 0)
                    {
                        textBox2.Text = textBox2.Text + "0000000" + " ";
                        l++;
                        k++;
                    }
                }
                else
                    j = 1;

                verif = message_code[i];
                while (verif < 1000000)	                                             //adauga 0 la numar in caz ca nu e de length=7
                {
                    textBox2.Text = textBox2.Text + 0;
                    verif *= 10;
                }
                textBox2.Text = textBox2.Text + message_code[i].ToString();
                textBox2.Text = textBox2.Text + " ";
                l++;
            }
        }

        private void timerDecrypting_Tick(object sender, EventArgs e)
        {
            while (toolStripProgressBar1.Value < toolStripProgressBar1.Maximum)
                toolStripProgressBar1.Value++;

            timerDecrypting.Enabled = false;
            labelDecrypting.Text = "Done!";
            textBox3.Visible = true;
            for (i = 0; i < message.Length; i += 7)
            {
                zecimal(int.Parse(message.Substring(i, 7)));
            }
        }
    }
}
